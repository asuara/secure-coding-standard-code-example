package com.positive.secure.coding.practices.practices;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LeastPrivilegeStandard extends PracticeStandard{

    static boolean secure = true;


    @Override
    public boolean itMyStandardNumber(int i) {
        return i==6 || i == 5;
    }

    @Override
    public Attacker getAttacker() {
        return LeastPrivilegeStandard::attacker;
    }

    static boolean receiveDataFromAttacker(Object obj){

        Data data = (Data) obj;
        if(secure){
            data.priviledges = new ArrayList<>(); // default deny
            data = setPriviledges(data);
        }else {
            data.priviledges.add("ALL");
        }
        return withdrawMoney(data);

    }

    private static boolean withdrawMoney(Data data){
        try{
            if(data.priviledges.contains(data.activity) || data.priviledges.contains("ALL")){
                logAttackerSuccessResponse("LeastPrivilegeStandard", "Attacker successfully accessd unpriviledge resources because of no least priviledge standard");
                return false;
            }else {
                throw new LeastPrivilegeStandardException();
            }
        }catch (LeastPrivilegeStandardException ex){
            logAttackerFailedResponse("LeastPrivilegeStandard", "Secure against no priviledge standard");
            return true;
        }
    }

    private static Data setPriviledges(Data data){
        String role = data.role;
        switch (role){
            case "MANAGER":
                data.priviledges.add("Can View");
                data.priviledges.add("Can Approve");
                data.priviledges.add("Can Decline");
                break;
            case "CUSTOMER":
                data.priviledges.add("Can Purchase");
                data.priviledges.add("Can Cancel Order");
                break;
            case "FINANCE OFFICER":
                data.priviledges.add("Withdraw");
                data.priviledges.add("Deposit");
                break;
        }
        return data;
    }

    private static boolean attacker(){
        Data data = new Data();
        data.role = "MANAGER";
        data.priviledges = new ArrayList<>();
        data.activity = "Withdraw";
        return receiveDataFromAttacker(data);
    }

    static class Data {
        String role;
        List<String> priviledges;
        String activity;
    }

    static class LeastPrivilegeStandardException extends Exception{
        LeastPrivilegeStandardException(){
            super("You don't have priviledge access to the complete the request");
        }
    }
}
