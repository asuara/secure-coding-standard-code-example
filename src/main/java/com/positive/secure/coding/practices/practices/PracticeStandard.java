package com.positive.secure.coding.practices.practices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class PracticeStandard {

    protected static boolean secure;

    public abstract boolean itMyStandardNumber(int i);

    public final boolean isPracticeCompliance(Attacker attacker){
        return attacker.attack();
    }

    public abstract Attacker getAttacker();

    interface Attacker {
        boolean attack();
    }

    protected static void logAttackerSuccessResponse(String klass, String message){
        Logger logger = LoggerFactory.getLogger(klass);
        logger.error(String.format("Attacker successfully attack %s leading to %s", klass, message));
    }

    protected static void logAttackerFailedResponse(String klass, String message){
        Logger logger = LoggerFactory.getLogger(klass);
        logger.error(String.format("Attacker failed to attack %s leading to %s", klass, message));
    }


}
