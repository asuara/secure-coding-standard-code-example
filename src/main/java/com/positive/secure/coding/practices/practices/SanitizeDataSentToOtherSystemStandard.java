package com.positive.secure.coding.practices.practices;

import org.springframework.stereotype.Service;

@Service
public class SanitizeDataSentToOtherSystemStandard extends PracticeStandard {

    protected static boolean secure = true;
    static boolean meetSystemCondition = false;

    @Override
    public boolean itMyStandardNumber(int i) {
        return  7 == i;
    }

    @Override
    public Attacker getAttacker() {
        return SanitizeDataSentToOtherSystemStandard::attack;
    }

    static void receiveDataFromAttacker(Object obj) throws SanitizeDataSentToOtherSystemStandardException{
        Data data = (Data) obj;
        if(secure){
            sanitizeDataBeforeSendingToOtherService(data);
        }
    }

    static void sanitizeDataBeforeSendingToOtherService(Data data) throws SanitizeDataSentToOtherSystemStandardException {
        if(!meetSystemCondition){
            throw new SanitizeDataSentToOtherSystemStandardException();
        }
    }

    static boolean attack(){
        Data data = new Data();
        data.age = 41;
        data.category = "ADULT";
        try{
            receiveDataFromAttacker(data);
            logAttackerSuccessResponse("SanitizeDataSentToOtherSystemStandard", "data sent to other service not sanitized by me. If it is malicious Other service is on its own");
            return false;
        }catch(SanitizeDataSentToOtherSystemStandardException ex){
            logAttackerFailedResponse("SanitizeDataSentToOtherSystemStandard", "data sent to other service is enfored sanitized by the system");
            return true;
        }
    }

    static class Data{
        int age;
        String category;
    }

    static class SanitizeDataSentToOtherSystemStandardException extends Exception{
        SanitizeDataSentToOtherSystemStandardException(){
            super("Data sent to Used service not sanitized");
        }
    }
}
