package com.positive.secure.coding.practices.practices;

import org.springframework.stereotype.Service;

@Service
public class SecurityInDepthStandard extends PracticeStandard{

    protected static boolean secure = true;

    static boolean loginProcess(Data data){
        boolean authenticated;
        if(secure){
            authenticated = data.loginSuccess && data.otpSuccess && data.isHttps;
        }else {
            authenticated =data.loginSuccess;
        }
        try{
            if(authenticated){
                logAttackerSuccessResponse("SecurityInDepthStandard", "Attacker successfully accessed without indepth authorization");
                return false;
            }else{
                throw new SecurityInDepthStandardException();
            }
        }catch (SecurityInDepthStandardException ex){
            logAttackerFailedResponse("SecurityInDepthStandard", "Attacker failed to accesses because of indepth layer authorization");
            return true;
        }
    }
    private static boolean attacker(){
        Data data = new Data();
        data.loginSuccess = true;
        data.isHttps= false;
        data.otpSuccess = false;
        return loginProcess(data);
    }

    @Override
    public boolean itMyStandardNumber(int i) {
        return i == 8;
    }

    @Override
    public Attacker getAttacker() {
        return SecurityInDepthStandard::attacker;
    }

    static class Data{
        boolean loginSuccess;
        boolean otpSuccess;
        boolean isHttps;
    }

    static class SecurityInDepthStandardException extends Exception{
        SecurityInDepthStandardException(){
            super("Security Indepth Exception fail to authenticate user at multiple layer of security");
        }
    }
}
