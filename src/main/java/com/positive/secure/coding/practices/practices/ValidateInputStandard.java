package com.positive.secure.coding.practices.practices;

import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ValidateInputStandard extends PracticeStandard{

    protected static boolean secure = true;

    private final static int maxDeposit = 2000;

    private static boolean attack() {
        Data invalidData = new Data();
        invalidData.timestamp = new Date(System.currentTimeMillis() + 9000000);
        invalidData.amount = 5000;
        try{
            receiveDataFromAttacker(invalidData);
            logAttackerSuccessResponse("ValidateInputStandard", "inconsistent data in the system");
            return false;
        }catch (ValidateInputException ex){
            logAttackerFailedResponse("ValidateInputStandard", "secure against non consistent data");
            return true;
        }
    }

    public static void receiveDataFromAttacker(Object obj) throws ValidateInputException {
        Data data = (Data) obj;
        if(secure){
            sanitizeData(data);
        }
    }

    private static void sanitizeData(Data data) throws ValidateInputException {
        if(data.timestamp.after(new Date()) || data.amount >maxDeposit){
            throw new ValidateInputException();
        }
    }

    @Override
    public boolean itMyStandardNumber(int i) {
        return 1== i;
    }

    @Override
    public Attacker getAttacker() {
        return ValidateInputStandard::attack;
    }

    static class Data {
        int amount;
        Date timestamp;
    }

    static class ValidateInputException extends Exception{
        ValidateInputException(){
            super("Your data are not consistence");
        }
    }
}
