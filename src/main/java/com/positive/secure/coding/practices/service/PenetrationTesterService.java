package com.positive.secure.coding.practices.service;

import com.positive.secure.coding.practices.practices.PracticeStandard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PenetrationTesterService {

    @Autowired
    private List<PracticeStandard> practiceStandardList;

   public boolean confirmIfAtLeastOneIsInsecured(){
       for(PracticeStandard standard : practiceStandardList){
           if(!standard.isPracticeCompliance(standard.getAttacker())) return false;
       }
       return true;
   }

    public boolean confirmIfAllAreSecured(){
        for(PracticeStandard standard : practiceStandardList){
            if(!standard.isPracticeCompliance(standard.getAttacker())) return false;
        }
        return true;
    }

    public boolean testByStandardNumber(int standardNumber) throws StandardNumberTesterNotAvailableException {
        PracticeStandard practiceStandard = getStandardByNumber(standardNumber);
        return practiceStandard.isPracticeCompliance(practiceStandard.getAttacker());
    }

    private PracticeStandard getStandardByNumber(int number) throws StandardNumberTesterNotAvailableException {
       for(PracticeStandard standard : practiceStandardList){
           if(standard.itMyStandardNumber(number)) return standard;
       }
       throw new StandardNumberTesterNotAvailableException();
    }

    public class StandardNumberTesterNotAvailableException extends Exception{
       StandardNumberTesterNotAvailableException(){
           super("This Standard Number Tester has not been implemented yet");
       }
    }

}
