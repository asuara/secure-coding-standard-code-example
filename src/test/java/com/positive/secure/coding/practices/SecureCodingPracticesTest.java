package com.positive.secure.coding.practices;

import com.positive.secure.coding.practices.service.PenetrationTesterService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = com.positive.secure.coding.practices.Application.class)
@WebAppConfiguration
public class SecureCodingPracticesTest {

    @Autowired
    private PenetrationTesterService penetrationTesterService;

    @Test
    public void allAreSecured(){
        Assert.assertTrue("All are not secured", penetrationTesterService.confirmIfAllAreSecured());
    }

    @Test
    public void atLeastOneIsInsecured(){
        Assert.assertTrue("At least one is insecured", penetrationTesterService.confirmIfAtLeastOneIsInsecured());
    }

    @Test
    public void testStandardOne() throws PenetrationTesterService.StandardNumberTesterNotAvailableException {
        Assert.assertTrue("Standard One is not met", penetrationTesterService.testByStandardNumber(1));
    }

    @Test
    public void testStandardFive() throws PenetrationTesterService.StandardNumberTesterNotAvailableException {
        Assert.assertTrue("Standard Five is not met", penetrationTesterService.testByStandardNumber(5));
    }

    @Test
    public void testStandardSix() throws PenetrationTesterService.StandardNumberTesterNotAvailableException {
        Assert.assertTrue("Standard Six is not met", penetrationTesterService.testByStandardNumber(6));
    }

    @Test
    public void testStandardSeven() throws PenetrationTesterService.StandardNumberTesterNotAvailableException {
        Assert.assertTrue("Standard Seven is not met", penetrationTesterService.testByStandardNumber(7));
    }

    @Test
    public void testStandardEight() throws PenetrationTesterService.StandardNumberTesterNotAvailableException {
        Assert.assertTrue("Standard Eight is not met", penetrationTesterService.testByStandardNumber(8));
    }

}
